# Copyright 2020 Amazon.com, Inc. or its affiliates. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License"). You
# may not use this file except in compliance with the License. A copy of
# the License is located at
#
#     http://aws.amazon.com/apache2.0/
#
# or in the "license" file accompanying this file. This file is
# distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF
# ANY KIND, either express or implied. See the License for the specific
# language governing permissions and limitations under the License.
"""Evaluation script for measuring model accuracy using an existing endpoint."""

import json
import logging
import os
import io
import boto3
import numpy as np
import pandas as pd

logger = logging.getLogger()
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler())

# May need to import additional metrics depending on what you are measuring.
# See https://docs.aws.amazon.com/sagemaker/latest/dg/model-monitor-model-quality-metrics.html
from sklearn.metrics import accuracy_score, classification_report, roc_auc_score, f1_score, fbeta_score

if __name__ == "__main__":
# definitions
    endpoint = 'mwe-fraud-classifier-prod'   # TODO: this is hardcoded only for testing; should be changed to the way deploy implements it
    CHUNK_LENGTH = 4096
    region = 'us-west-2'
    boto_session = boto3.Session(region_name=region)

    print("Loading test input data")
    test_path = "/opt/ml/processing/test/test.csv"
    csv_buffer = open(test_path)
    my_payload_as_csv = csv_buffer.read()
    df = pd.read_csv(test_path, header=None)

    logger.debug("Reading test data.")
    y_test = df.iloc[:, 0].to_numpy()
    df.drop(df.columns[0], axis=1, inplace=True)

    logger.info(f"Performing predictions against test data for endpoint {endpoint}.")
    
    start = 0
    runtime_client = boto_session.client("sagemaker-runtime")
    predictions = np.array([])
    while start<df.shape[0]:
        payload = io.StringIO()
        df.iloc[start:min(start+CHUNK_LENGTH,df.shape[0])].to_csv(payload, header=None, index=None)
        start += CHUNK_LENGTH
        try:
            response = runtime_client.invoke_endpoint(
                EndpointName=endpoint,
                Body=payload.getvalue(),
                ContentType='text/csv',
                Accept='Accept')
            output = response['Body'].read().decode('utf-8')
            predictions=np.concatenate([predictions,np.fromstring(output, dtype=float, sep=',')],axis=None)
        except:
            print(f'EndpointEvaluation.py error getting information from endpoint {endpoint}')
    
    # only pass if we have the same number of inferences as ground truths... i.e. did the endpoint work?
    if (len(predictions)==len(y_test)):

        print("Creating classification evaluation report")
        acc = accuracy_score(y_test, predictions.round())
        auc = roc_auc_score(y_test, predictions.round())
        f1 = f1_score(y_test, predictions.round())
        f2 = fbeta_score(y_test, predictions.round(), beta=2)

        # The metrics reported can change based on the model used, but it must be a specific name per (https://docs.aws.amazon.com/sagemaker/latest/dg/model-monitor-model-quality-metrics.html)
        report_dict = {
            "binary_classification_metrics": {
                "accuracy": {
                    "value": acc,
                    "standard_deviation": "NaN",
                },
                "auc": {"value": auc, "standard_deviation": "NaN"},
                "f1": {"value": f1, "standard_deviation": "NaN"},
                "f2": {"value": f2, "standard_deviation": "NaN"},
            },
        }
    else:
        report_dict = {
            "binary_classification_metrics": {
                "accuracy": {
                    "value": "NaN",
                    "standard_deviation": "NaN",
                },
                "auc": {"value": "NaN", "standard_deviation": "NaN"},
                "f1": {"value": "NaN", "standard_deviation": "NaN"},
                "f2": {"value": "NaN", "standard_deviation": "NaN"},
            },
        }

    print("Classification report:\n{}".format(report_dict))

    evaluation_output_path = os.path.join("/opt/ml/processing/evaluation", "evaluation_endpoint.json")
    print("Saving classification report to {}".format(evaluation_output_path))

    with open(evaluation_output_path, "w") as f:
        f.write(json.dumps(report_dict))